package com.example

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.Http
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import akka.stream.ActorFlowMaterializer
import scala.concurrent.duration._

object Main extends TestService {
  override implicit val system = ActorSystem()
  override implicit val executor = system.dispatcher
  override implicit val materializer = ActorFlowMaterializer()
  override implicit val timeOut = Timeout(10 second)

  override val config = ConfigFactory.load()
  override val logger = Logging(system, getClass)

  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
