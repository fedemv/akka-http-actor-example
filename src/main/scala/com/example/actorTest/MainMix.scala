package com.example.actorTest

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.Http
import akka.stream.ActorFlowMaterializer
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import scala.concurrent.duration._

object MainMix extends App with HealthService {
  implicit val config = ConfigFactory.load()
  implicit val system = ActorSystem()
  implicit def executor = system.dispatcher
  implicit val materializer = ActorFlowMaterializer()
  implicit val logger: LoggingAdapter = Logging(system, getClass)
  override implicit val timeOut = Timeout(10 second)

  Http(system).bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
