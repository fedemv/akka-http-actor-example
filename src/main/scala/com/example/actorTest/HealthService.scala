package com.example.actorTest

import akka.actor.{ActorLogging, Actor, Props}
import akka.http.marshalling.ToResponseMarshallable
import akka.http.server.Directives._
import com.example.Service
import akka.pattern.ask

trait HealthService extends Service {

  import MessageTextFormat._

  //see more https://github.com/spray/spray/wiki/Parameter-Filters
  val routes = {
    logRequestResult("example") {
      path("health") {
        get {
          cx => {
            val message = MessageText("healthy")
            cx.complete(ToResponseMarshallable(message))
          }
        }
      } ~
      pathPrefix("healthWithActor") {
        pathEnd {
          get {
            complete {
              val firstActor = system.actorOf(Props[FirstActor])
              (firstActor ? "algo").mapTo[MessageText]
            }
          }
        } ~
        (get & path(IntNumber) & parameter("name"?)){
          (age, nameOpt) => {
            complete{
              val firstActor = system.actorOf(Props[FirstActor])
              logger.info(nameOpt.toString)
              (firstActor ? (if (nameOpt.isDefined) MessageNameAge(nameOpt.get, age) else MessageText("carlitos"))).mapTo[MessageText]
            }
          }
        } ~
        (get & path(Segment)) {
          name => {
            complete {
              val firstActor = system.actorOf(Props[FirstActor])
              (firstActor ? MessageText(name)).mapTo[MessageText]
            }
          }
        } ~
        (get & path( Segment / IntNumber)) {
          (name, age) => {
            complete {
              val firstActor = system.actorOf(Props[FirstActor])
              (firstActor ? MessageNameAge(name, age)).mapTo[MessageText]
            }
          }
        }
      }
    }
  }
}

class FirstActor extends Actor with ActorLogging {
  override def receive = {
    case MessageText(name) => sender ! MessageText(s"hello $name")
    case MessageNameAge(name, age) => sender ! MessageText(s"hello $name, you're ${age.toString}")
    case r => sender ! MessageText("healthWithActor")
  }
}
