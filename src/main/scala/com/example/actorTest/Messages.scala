package com.example.actorTest

import akka.http.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class MessageText(text: String)
case class MessageNameAge(name: String, age: Int)

object MessageTextFormat extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val messagesTextFormat = jsonFormat1(MessageText.apply)
}
