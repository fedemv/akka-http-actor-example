#!/bin/sh

java -Xmx1G -XX:MaxPermSize=2G -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 -jar $SBT_HOME/bin/sbt-launch.jar $@
